::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                       Generador de módulos geco							    ::
::                                                                              ::
:: Use this template to generate a geco module.						            ::
:: Soporta: modulos ya existentes, nuevos modulos                               ::
::                                                                              ::
:: Change History                                                               ::
:: 01/12/2022  Ronny Medina - Init Script										::
::                                                                              ::
::                                                                              ::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::                                                                              ::
::  Copyright (C) 2007, 2019 David Both                                         ::
::  ronny.medina-eras@capgemini.com                                             ::
::                                                                              ::
::  This program is free software; you can redistribute it and/or modify        ::
::  it under the terms of the GNU General Public License as published by        ::
::  the Free Software Foundation; either version 2 of the License, or           ::
::  (at your option) any later version.                                         ::
::                                                                              ::
::  This program is distributed in the hope that it will be useful,             ::
::  but WITHOUT ANY WARRANTY; without even the implied warranty of              ::
::  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               ::
::  GNU General Public License for more details.                                ::
::                                                                              ::
::  You should have received a copy of the GNU General Public License           ::
::  along with this program; if not, write to the Free Software                 ::
::  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   ::
::                                                                              ::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@echo off
cls 
TITLE Bienvenid@ %USERNAME% al Generador de modulos Geco
MODE con:cols=80 lines=40

:inicio
SET var=0
cls
echo ------------------------------------------------------------------------------
echo  %DATE% ^| %TIME%
echo ------------------------------------------------------------------------------
echo  1    Modulo FPCA
echo  2    Modulo Content Type
echo  3    Salir
echo ------------------------------------------------------------------------------
echo.

SET /p var= ^> Seleccione una opcion [1-3]:

if "%var%"=="0" goto inicio
if "%var%"=="1" goto fpca
if "%var%"=="2" goto content
if "%var%"=="3" goto salir

::Mensaje de error, validación cuando se selecciona una opción fuera de rango
echo. El numero "%var%" no es una opcion valida, por favor intente de nuevo.
echo.
pause
echo.
goto:inicio

:fpca
    echo.
    echo. Has elegido la opcion FPCA
    echo.
        color 2
    SET /p fpca= ^> Nombre de la fpca? 
    SET /p path= ^> Directorio del modulo? 
    SET /p package= ^> Paquete del modulo? 
    
    set rootPath=%path%\%package%
    echo. Comprobando Directorio %rootPath%

    if exist %rootPath%\ (goto:buildfpca) else (goto:create)
    :create
    	echo. Generando modulo nuevo
    	mkdir %rootPath%
    	mkdir %rootPath%\formatters
    	mkdir %rootPath%\formatters_config
    	mkdir %rootPath%\i18n
    	type nul > %rootPath%\i18n\%package%
    	type nul > %rootPath%\i18n\%package%_ca
    	type nul > %rootPath%\i18n\%package%_es
    	type nul > %rootPath%\i18n\%package%_es
    	type nul > %rootPath%\i18n\%package%_oc
    	mkdir %rootPath%\resources
    	type nul > %rootPath%\manifest.xml  	
    	goto:buildfpca

    :buildfpca
    	echo. construyendo la estructura para la fpca %fpca%
    	type nul > %rootPath%\formatters\%fpca%.jsp
    	type nul > %rootPath%\formatters_config\%fpca%.properties
    	mkdir %rootPath%\resources\%fpca%
    	mkdir %rootPath%\resources\%fpca%\js
    	type nul > %rootPath%\resources\%fpca%\js\%fpca%.js
    	mkdir %rootPath%\resources\%fpca%\css
    	type nul > %rootPath%\resources\%fpca%\css\%fpca%.css
    	mkdir %rootPath%\resources\%fpca%\img
    	::TODO templete for jsp, properties config, js and css
    echo.
    pause
    goto:inicio

:content
    echo.
    echo. Has elegido la opcion No. 2
    echo.
        ::Aquí van las líneas de comando de tu opción
        color 4
    echo.
    pause
    goto:inicio

:salir
	echo. Cerrando programa...
	pause
    @cls&exit

::set /p absolutePath="Ruta absoluta del Framework: "
::set /p moduleName="Nombre del paquete del modulo: "





